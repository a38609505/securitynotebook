package com.example.securitynotebook.repository;



import com.example.securitynotebook.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    long countByUsername(String username);
}
