package com.example.securitynotebook.service;

import com.example.securitynotebook.model.common.ListResult;
import com.example.securitynotebook.model.dataTest.DataTestDetailResponse;
import com.example.securitynotebook.model.dataTest.DataTestListItem;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class DataTestService {
    public ListResult<DataTestListItem> getList() {
        DataTestListItem item1 = new DataTestListItem();
        item1.setId(1L);
        item1.setName("홍길동");
        item1.setPhone("010-0000-0000");

        DataTestListItem item2 = new DataTestListItem();
        item2.setId(2L);
        item2.setName("홍길동2");
        item2.setPhone("010-0000-0002");

        List<DataTestListItem> listItems = new LinkedList<>();
        listItems.add(item1);
        listItems.add(item2);

        return ListConvertService.settingResult(listItems);
    }

    public DataTestDetailResponse getDetail() {
        DataTestDetailResponse response = new DataTestDetailResponse();
        response.setId(1L);
        response.setName("홍길동");
        response.setPhone("010-0000-0000");
        response.setContents("gkgkgkgkgk");

        return response;
    }
}
