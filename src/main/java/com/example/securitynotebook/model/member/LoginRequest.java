package com.example.securitynotebook.model.member;

import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;



@Getter
@Setter
public class LoginRequest {
    @ApiModelProperty(notes = "아이디", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String username;

    @ApiModelProperty(notes = "비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;
}
