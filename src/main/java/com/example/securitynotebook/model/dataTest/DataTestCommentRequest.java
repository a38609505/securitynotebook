package com.example.securitynotebook.model.dataTest;

import io.swagger.annotations.ApiModelProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;


@Getter
@Setter
public class DataTestCommentRequest {
    @ApiModelProperty(notes = "댓글", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String comment;
}
