package com.example.securitynotebook.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
