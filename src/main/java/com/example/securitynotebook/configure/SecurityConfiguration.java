package com.example.securitynotebook.configure;

import lombok.RequiredArgsConstructor;
import org.apache.catalina.filters.CorsFilter;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration // 기존 security 설정 방법
@RequiredArgsConstructor // 기존 security 설정 방법, 이 Annotation(어노테이션)은 필요가 없어보임.
@EnableWebSecurity
/*
3버전부터 바뀐 security 설정 방법,
기본 보안 구성을 비황성화하는 경우 매우 중요,
응용 프로그램이 없으면 응용 프로그램이 시작되지 않음.
*/
@EnableMethodSecurity // 3버전부터 추가된 security 설정 방법
public class SecurityConfiguration { // extends WebSecurityConfigurerAdapte 기존에 있던것이 이제는 사용이 불가하여 삭제
                                    // 앞으로는 AutoWired, Override를 사용 불가능해 보임.


    /*
    이것은 필요할것 같음 그래서 남겨 놓기로 결정.
    이유는 swagger접속을 하기 위해서는 필요하다고 생각을 하는중.
    */
    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    /*
    기존코드에 있는것.
    httpBasic
    csrf
    session
    header
    cors는 맨 밑에 있음.
    */

    // 기존에 있던 extends WebSecurityConfigurerAdapte를 사용이 불가하게 되면서 public으로 사용


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic(AbstractHttpConfigurer::disable) // http basic auth기반으로 로그인 인증창이 뜸(기본인증 로그인을 이용하지 않으면 disable())
                .csrf(AbstractHttpConfigurer::disable) // html tag를 통한 공격(api서버 이용시 disable())
                .cors(Customizer.withDefaults())
                .headers(header -> header.frameOptions(HeadersConfigurer.FrameOptionsConfig::disable))
                .authorizeHttpRequests( // 각 경로 path별 권한 처리
                registry // registry -> 기재 ( 이곳부터는 기존 security와 비슷함. )
                        -> registry
                        .requestMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체허용
                        .requestMatchers("/v1/member/login/**").permitAll() // 전체허용
                        .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN") // 관리자허용
                        .requestMatchers("/v1/auth-test/test-user").hasAnyRole("USER") // 유저허용
                        .requestMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN, USER") // 관리자, 유저 허용
                        .requestMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN, USER") // 관리자, 유저 허용
                        .anyRequest().hasRole("ADMIN")

                );
        return httpSecurity.build();
    }


    /*
    패스워드 암호화 관련 메소드, BCryptPasswordEncoder
    (BCryptPasswordEncoder 인코딩을 통하여 비밀번호에 대한 암호화를 수행한다.)
    password를 암호화 해줌
    필요하다고 하여 가져오긴 하였으나 Application에도 존재.. 굳이 필요없어보임..
    인터넷에서는 BCrypt를 더 가져오라 강조 허나 Application에 PasswordEncoder가 존재..
    */
//  @Bean
//  public PasswordEncoder passwordEncoder() {
//      return new BCryptPasswordEncoder();
//  }



    // Protected냐 public냐 문제로다...

//      @Bean
//      public PasswordEncoder passwordEncoder() {
//          return PasswordEncoderFactories.createDelegatingPasswordEncoder();
//       }
//
//    @Bean

//    protected SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity.csrf().disable();
//        httpSecurity.headers().frameOptions().disable();
//        httpSecurity.authorizeHttpRequests(authorize -> authorize
//                .requestMatchers(PathRequest.toH2Console()).permitAll() // 전체허용
//                .requestMatchers("/v1/member/login/**").permitAll() // 전체허용
//                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN") // 관리자만 허용
//                .requestMatchers("/v1/auth-test/test-user").hasAnyRole("USER") // 유저만 허용
//                .requestMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN, USER") // 관리자, 유저 허용
//                .requestMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER") // 관리자 유저 허용
//                .anyRequest().authenticated()
//        );
//        return httpSecurity.build();
//    }



//     인터넷을 두져보며 열심히 만들어본 security ( 하지만 이건 도저히 아닌것 같음 )
//     기존것과 최대한 유사하게 만들어 보았음..
//
//    @Bean
//    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
//
//        httpSecurity
//                .csrf((csrfConfig) ->
//                        csrfConfig.disable()
//                ) // 1번
//                .headers((headersConfig) ->
//                        headersConfig.frameOptions(frameOptionsConfig ->
//                                frameOptionsConfig.disable()
//                        )
//                ) // 2번
//                .authorizeHttpRequests((authorizeRequests) ->
//                        authorizeRequests
//                                .requestMatchers(PathRequest.toH2Console()).permitAll() // 전체허용
//                                .requestMatchers("/v1/member/login/**").permitAll() // 전체허용
//                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN") // 관리자만 허용
//                                .requestMatchers("/v1/auth-test/test-user").hasAnyRole("USER") // 유저만 허용
//                                .requestMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER") // 관리자 유저 허용
//                                .requestMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER") // 관리자 유저 허용
//                                .anyRequest().authenticated()
//                ) // 3번
//                .exceptionHandling((exceptionConfig) ->
//                        exceptionConfig.authenticationEntryPoint(unauthorizedEntryPoint).accessDeniedHandler(accessDeniedHandler)
//                )
//        )
//        return httpSecurity.build();
//    }







    // 기존에 사용해왔던 security (이제는 사용 불가능)

//    @Override
//    public void configure(WebSecurity webSecurity) throws Exception {
//        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
//    }
//
//    @Bean
//    @Override
//    public AuthenticationManager authenticationManager() throws Exception {
//        return super.authenticationManagerBean();
//    }
//
//    /*
//    flutter 에서 header에 token 넣는법
//    String? token = await TokenLib.getToken();
//
//    Dio dio = Dio();
//    dio.options.headers['Authorization'] = 'Bearer ' + token!; <-- 한줄 추가
//
//    --- 아래 동일
//     */
//
//    // 퍼미션이 무엇인지 보기.
//    @Override
//    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity
//                .httpBasic().disable()
//                .csrf().disable()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                    .authorizeRequests()
//                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용
//                        .antMatchers("/v1/member/login/**").permitAll() // 전체허용
//                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
//                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
//                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER")
//                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")
//                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
//                .and()
//                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
//                .and()
//                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
//                .and()
//                    .addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
//    }
//


    @Bean
    public CorsConfigurationSource corsConfigurationSource() { // 킥보드를 보며 수정하였음.
        CorsConfiguration configuration = new CorsConfiguration();


        configuration.addAllowedOriginPattern("*");
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
