package com.example.securitynotebook.controller;

import com.example.securitynotebook.entity.Member;
import com.example.securitynotebook.enums.MemberGroup;
import com.example.securitynotebook.model.common.CommonResult;
import com.example.securitynotebook.model.member.MemberCreateRequest;
import com.example.securitynotebook.service.MemberDataService;
import com.example.securitynotebook.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.Response;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberDataService memberDataService;

    @ApiOperation(value = "회원가입") // 임시로 만든 컨트롤러
    @PostMapping("/join")
    public CommonResult setMember(@RequestBody @Valid MemberCreateRequest memberCreateRequest) {
        Member member = memberDataService.setMember(MemberGroup.ROLE_USER, memberCreateRequest);
        return ResponseService.getSuccessResult();
    }
}
